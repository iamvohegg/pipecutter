# Pipe Cutter

*A Handy Tool to Get the Job Done*

## Introduction

My most recent hardware project involved creating a rack for my car to store all my gearboxes. I designed a simple structure using aluminum pipes and connectors, which prompted me to compile a list of the necessary pipes and connectors. These items come in various lengths and are priced differently in stores. I needed to determine how many I should purchase and what lengths could be cut from a single bar. What if I bought 100 centimeters, 160, or 260? How much do I need, and how much will be wasted? As a solution, I decided to develop a tool for this purpose, not only to perform the necessary calculations but also to enhance my PySimpleGUI skills.## Feature Overview

The tool is user-friendly. It gathers information from the user, including the length of available pipes or wood and whether they wish to factor in the width of the saw blade. Subsequently, the user compiles a list of items they intend to cut, specifying both the desired length (in meters, centimeters, feet, inches, or any preferred unit) and the quantity needed. For instance, let's take the example of a picture frame measuring 30 by 60 centimeters. In this case, you'd need 30 twice and 60 twice. If your only option is to purchase 100-centimeter wooden rods, you can either buy 4 of them or make do with 2.
## Table of Contents

* [Getting Started](#getting-started)
    * [Requirements](#requirements)
    * [Installation](#installation)
    * [Usage](#usage)

## Getting Started

Interested in giving it a try? Simply download the Python file, run it, and explore the tool.

### Requirements

You'll need Python installed (I used version 3.11 in this case, but any 3.x should work) and PySimpleGUI.

### Installation

There's no need to install anything, just download and run the script. If you don't have PySimpleGUI installed in your Python environment, you can do so with the following commands:

```bash
pip install pysimplegui
```
# usage
Just check that out.
![pipcutter.png](pipcutter.png)

On top you see the required length and how many of them. Every time you hit a button or change a value the list on the bottom
be updatet and show the pieces each raw bar is cut to. Here it be 5 pipes .
