#! /usr/bin/env python3
# -*- coding: utf-8 -*-

"""Template for python3 terminal scripts.
This gist allows you to quickly create a functioning
python3 terminal script using argparse and subprocess.
"""
import re

import PySimpleGUI as sgui
# https://pyfpdf.readthedocs.io/en/latest/
import fpdf  # pip3 install fpdf

__author__ = "Volker Heggemann"
__copyright__ = "Copyright 2023, Volker Heggemann"
__credits__ = ["Volker Heggemann"]
__license__ = "CC BY-NC-SA 4.0 DEED"
__version__ = "0.1.1"
__maintainer__ = "volker heggemann"
__email__ = "v.heggemann@gmx.net"
__status__ = "as_stable_as_possible"


def get_first_num(sample):
    match = re.search(r'\d+', sample)
    return match.group()


def make_pdf_ofthat(parentw, parameters):
    filename = sgui.tk.filedialog.asksaveasfilename(
        defaultextension='pdf',
        filetypes=(("All TXT Files", "*.pdf"), ("All Files", "*.*")),
        initialdir='~/pdf_out/',
        initialfile='out.pdf',  # Option added here
        parent=parentw.TKroot,
        title="Save As"
    )
    pdflines = []
    for line in parameters:
        for subline in str(line).split(', '):
            pdflines.append(subline)

    if filename:
        pdf = fpdf.FPDF(format='A4')  # pdf format
        pdf.add_page()  # create new page

        pdf.set_font("Arial", size=12)  # font and textsize
        pdf.cell(200, 10, txt=str(f'Ausdruck von {__name__}'), ln=1, align="L")
        pdf.cell(200, 10, txt=f'Pipcutter V{__version__}', ln=1, align="L")
        pdf.cell(200, 10, txt=f'Stücke und Anzahl:', ln=1, align="L")
        pdf.cell(200, 10, txt=f'sowie Längen und Schnitte', ln=1, align="L")

        for line in pdflines:
            pdf.cell(200, 10, txt=str(line).replace(",", "\n"), ln=1, align="L")
        pdf.output(filename)


def calc_cuttinglist(raw_length, saw_width, list_of_pieces_wanted, linewithgiven):
    required_raw_length = 1
    actual_rest = raw_length
    ok = False
    blended_rest = 0.0
    listofdata = []
    listofshapes = []
    shapes = []
    blade = float(saw_width)
    if not list_of_pieces_wanted:
        return
    for i in sorted(list_of_pieces_wanted):
        for count in range(int(i[1])):
            listofdata.append(float(i[0]))
    if max(listofdata) > raw_length:
        sgui.popup_error('Error you want larger pieces then you have raw material for!')
        return []
    while True:
        for i in sorted(listofdata, reverse=True):
            if actual_rest - i >= 0.0:
                # print(f"{required_raw_length} pipe, even {actual_rest:.2f} cut now: {i}")
                actual_rest = actual_rest - i - blade
                listofdata.remove(i)
                ok = True
                shapes.append(i)
                break
        if not ok:
            listofshapes.append(shapes)
            shapes = []
            required_raw_length += 1
            blended_rest += actual_rest
            # print(f"{required_raw_length} Stange, blended_rest {actual_rest:.2f} required_raw_lengthblended_rest {
            # blended_rest:.2f}")
            actual_rest = raw_length
        ok = False
        anzahl = 0
        for i in sorted(listofdata, reverse=True):
            anzahl += i
        if anzahl == 0:
            listofshapes.append(shapes)
            break

    blended_rest += actual_rest
    # print(f"{required_raw_length} Stange, blended_rest {actual_rest:.2f} required_raw_lengthblended_rest {
    # blended_rest:.2f}")

    hunderprozent = raw_length
    output = []
    for shapes in listofshapes:
        line = '|'
        linesum = 0.0
        actual_rest = linewithgiven
        for i in shapes:
            linesum += i
            w = round(((100.0 / hunderprozent) * i) * (linewithgiven / 100))
            actual_rest -= w
            line += f'{i:.2f}|'.rjust(w, '-')
        line += '|'.rjust(int(actual_rest), '_')
        output.append(line + str(linesum))
    return output


def eventloop():
    pieces = []  # holds an in a list, key,value lists (like [[length,count],[a,b],[c,b]])
    pieces_lst = sgui.Listbox(values=pieces, expand_x=True, enable_events=True, size=(10, 10), key="-PIECES_LST-")
    linewith = 80
    cutted = []
    cutted_lst = sgui.Listbox(values=cutted, expand_x=True, expand_y=True, enable_events=True, size=(linewith, 10),
                              key="-CUTTED_LST-")

    layout = [[sgui.Text("Some data input:")],
              [sgui.Text("Raw length of the bar that is to be sawn "),
               sgui.Input(default_text='200', key='-RAW_LENGTH-', size=(10, 1), justification='right'),
               sgui.Push(),
               sgui.Text("Saw blade width? "),
               sgui.Input(default_text='0.0', key='-SAW_BLADE-', enable_events=True, size=(10, 1),
                          justification='right')],
              [sgui.Input(default_text='20.5', key='-PIECE_LEN-', size=(10, 1), justification='left'),
               sgui.Text("lenght i need so often:"),
               sgui.Push(),
               sgui.Input(default_text='2', key='-PIECE_COUNT-', size=(10, 1), justification='right'),
               sgui.Button('add', key='-BUTTON_ADD-')],
              [pieces_lst],
              [sgui.Text("Remove selected line from the list "),
               sgui.Push(),
               sgui.Button('del', key='-BUTTON_DEL-')],
              [sgui.Text("Pieces you need ")],
              [cutted_lst],
              [sgui.Button('ok', key='OK'),
               sgui.Push(),
               sgui.Button('PDF export', key='pdf'),
               sgui.Push(),
               sgui.Button('Exit', key='Exit')],
              ]

    # sgui.theme('DarkGrey6')

    window = sgui.Window(f"Cutting Tool {__version__}", layout=layout, size=(800, 640))

    while True:
        changes = False
        event, values = window.read()
        if event == "Exit" or event == sgui.WIN_CLOSED:
            break
        if event == "pdf":
            make_pdf_ofthat(window, [pieces,
                                     calc_cuttinglist(eval(values['-RAW_LENGTH-']), eval(values['-SAW_BLADE-']), pieces,
                                                      linewith)])

        if event == "-BUTTON_ADD-":
            piecelen = values['-PIECE_LEN-']
            piecelen = piecelen.replace(',', '.')
            piecelen = piecelen.rjust(20, " ")
            piececount = get_first_num(values['-PIECE_COUNT-']).rjust(10, " ")
            pieces.append([piecelen, piececount])
            window["-PIECES_LST-"].update(pieces)
            changes = True
        if event == "-BUTTON_DEL-":
            if not values['-PIECES_LST-']:
                sgui.Popup('Error', 'You must select a Country')
            else:
                # find and remove this
                pieces.remove(values['-PIECES_LST-'][0])
            window["-PIECES_LST-"].update(pieces)
            changes = True
        if event == "-RAW_LENGTH-":
            changes = True
        if event == "-SAW_BLADE-":
            changes = True
        if changes or event == "OK":
            cutted = calc_cuttinglist(eval(values['-RAW_LENGTH-']), eval(values['-SAW_BLADE-']), pieces, linewith)
            window["-CUTTED_LST-"].update(cutted)

    window.close()


def main():
    sgui.popup_timed('This is Cutting Tool. It helps you to get the number of needed length (from a thing you want to '
                     'cut e.g.:) pipes. You have to give the lenght of the raw (e.g) pipe and all the pieces you want'
                     ' to cut. \nYou get a cutting list back. ', title='Cutting Tool', auto_close_duration=5)
    eventloop()


if __name__ == '__main__':
    main()
